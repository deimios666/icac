/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.Operator;

/**
 *
 * @author Deimios
 */
@Local
public interface OperatorFacadeLocal {

    void create(Operator operator);

    void edit(Operator operator);

    void remove(Operator operator);

    Operator find(Object id);

    List<Operator> findAll();

    List<Operator> findRange(int[] range);

    int count();
    
}
