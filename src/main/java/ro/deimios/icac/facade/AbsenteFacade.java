/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.icac.entity.Absente;

/**
 *
 * @author Deimios
 */
@Stateless
public class AbsenteFacade extends AbstractFacade<Absente> implements AbsenteFacadeLocal {

    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AbsenteFacade() {
        super(Absente.class);
    }

    @Override
    public List<Absente> findByAnLunaSirues(int anul, int luna, long sirues) {
        Query query = em.createNamedQuery("Absente.findByAnLunaSirues");
        query.setParameter("anul", anul);
        query.setParameter("luna", luna);
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }
    
    @Override
    public void deleteByAnLunaSirues(int anul, int luna, long sirues) {
        Query query = em.createNamedQuery("Absente.deleteByAnLunaSirues");
        query.setParameter("anul", anul);
        query.setParameter("luna", luna);
        query.setParameter("sirues", sirues);
        query.executeUpdate();
    }
    
}
