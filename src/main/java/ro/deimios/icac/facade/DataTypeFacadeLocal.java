/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.DataType;

/**
 *
 * @author Deimios
 */
@Local
public interface DataTypeFacadeLocal {

    void create(DataType dataType);

    void edit(DataType dataType);

    void remove(DataType dataType);

    DataType find(Object id);

    List<DataType> findAll();

    List<DataType> findRange(int[] range);

    int count();
    
}
