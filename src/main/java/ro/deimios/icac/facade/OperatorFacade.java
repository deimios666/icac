/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.icac.entity.Operator;

/**
 *
 * @author Deimios
 */
@Stateless
public class OperatorFacade extends AbstractFacade<Operator> implements OperatorFacadeLocal {
    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OperatorFacade() {
        super(Operator.class);
    }
    
}
