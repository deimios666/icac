/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.icac.entity.Retea;

/**
 *
 * @author Deimios
 */
@Stateless
public class ReteaFacade extends AbstractFacade<Retea> implements ReteaFacadeLocal {

    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReteaFacade() {
        super(Retea.class);
    }

    @Override
    public List<Retea> findAllPJ() {
        Query query = em.createNamedQuery("Retea.findAllPJ");
        return query.getResultList();
    }

    @Override
    public List<Retea> findAllPJForOperator(long operatorId) {
        Query query = em.createNamedQuery("Retea.findAllPJForOperator");
        query.setParameter("operatorId", operatorId);
        return query.getResultList();
    }

    @Override
    public List<Retea> findAllARForPJ(long sirues) {
        Query query = em.createNamedQuery("Retea.findAllARForPJ");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }
}
