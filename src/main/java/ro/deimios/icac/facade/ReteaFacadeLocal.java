/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.Retea;

/**
 *
 * @author Deimios
 */
@Local
public interface ReteaFacadeLocal {

    void create(Retea retea);

    void edit(Retea retea);

    void remove(Retea retea);

    Retea find(Object id);

    List<Retea> findAll();

    List<Retea> findRange(int[] range);

    int count();

    public java.util.List<ro.deimios.icac.entity.Retea> findAllPJ();

    public java.util.List<ro.deimios.icac.entity.Retea> findAllPJForOperator(long operatorId);

    public java.util.List<ro.deimios.icac.entity.Retea> findAllARForPJ(long sirues);
}
