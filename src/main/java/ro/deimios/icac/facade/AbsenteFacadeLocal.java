/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.Absente;

/**
 *
 * @author Deimios
 */
@Local
public interface AbsenteFacadeLocal {

    void create(Absente absente);

    void edit(Absente absente);

    void remove(Absente absente);

    Absente find(Object id);

    List<Absente> findAll();

    List<Absente> findRange(int[] range);

    int count();

    public java.util.List<ro.deimios.icac.entity.Absente> findByAnLunaSirues(int anul, int luna, long sirues);

    public void deleteByAnLunaSirues(int anul, int luna, long sirues);
}
