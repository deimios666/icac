/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.icac.entity.Completat;

/**
 *
 * @author Deimios
 */
@Stateless
public class CompletatFacade extends AbstractFacade<Completat> implements CompletatFacadeLocal {

    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompletatFacade() {
        super(Completat.class);
    }

    @Override
    public Completat findByLunaAnSirues(long sirues, int luna, int anul) {
        Query query = em.createNamedQuery("Completat.findByLunaAnSirues");
        query.setParameter("sirues", sirues);
        query.setParameter("luna", luna);
        query.setParameter("anul", anul);
        List<Completat> result = query.getResultList();
        if (result != null && result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public void deleteByLunaAnSirues(long sirues, int luna, int anul){
        Query query = em.createNamedQuery("Completat.findByLunaAnSirues");
        query.setParameter("sirues", sirues);
        query.setParameter("luna", luna);
        query.setParameter("anul", anul);
        query.executeUpdate();
        List<Completat> result = query.getResultList();
        for (Completat c: result){
            em.remove(c);
        }
    }
    
 
    @Override
    public void deleteByLunaAnSiruesPJ(long sirues, int luna, int anul){
        Query query = em.createNamedQuery("Completat.findByLunaAnSiruesPJ");
        query.setParameter("sirues", sirues);
        query.setParameter("luna", luna);
        query.setParameter("anul", anul);
        List<Completat> result = query.getResultList();
        for (Completat c: result){
            em.remove(c);
        }
    }
    
}
