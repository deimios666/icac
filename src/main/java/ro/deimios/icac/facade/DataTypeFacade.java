/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.icac.entity.DataType;

/**
 *
 * @author Deimios
 */
@Stateless
public class DataTypeFacade extends AbstractFacade<DataType> implements DataTypeFacadeLocal {
    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DataTypeFacade() {
        super(DataType.class);
    }
    
}
