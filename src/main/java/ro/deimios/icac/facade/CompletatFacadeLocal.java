/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.Completat;

/**
 *
 * @author Deimios
 */
@Local
public interface CompletatFacadeLocal {

    void create(Completat completat);

    void edit(Completat completat);

    void remove(Completat completat);

    Completat find(Object id);

    List<Completat> findAll();

    List<Completat> findRange(int[] range);

    int count();

    public ro.deimios.icac.entity.Completat findByLunaAnSirues(long sirues, int luna, int anul);

    public void deleteByLunaAnSirues(long sirues, int luna, int anul);

    public void deleteByLunaAnSiruesPJ(long sirues, int luna, int anul);
    
}
