package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.icac.entity.Users;

/**
 *
 * @author Deimios
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> implements UsersFacadeLocal {

    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    @Override
    public Users findByUsername(String username) {
        Query query = em.createNamedQuery("Users.findByUsername");
        query.setParameter("username", username);
        List<Users> userList = query.getResultList();
        if (userList != null && userList.size() == 1) {
            return userList.get(0);
        } else {
            return null;
        }
    }
}
