/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.icac.entity.Users;

/**
 *
 * @author Deimios
 */
@Local
public interface UsersFacadeLocal {

    void create(Users users);

    void edit(Users users);

    void remove(Users users);

    Users find(Object id);

    List<Users> findAll();

    List<Users> findRange(int[] range);

    int count();

    public ro.deimios.icac.entity.Users findByUsername(java.lang.String username);
    
}
