package ro.deimios.icac.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import ro.deimios.icac.entity.Users;
import ro.deimios.icac.facade.UsersFacadeLocal;

/**
 *
 * @author Deimios
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {
    
    @EJB
    private UsersFacadeLocal usersFacade;
    Users user;

    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }
    
    public Users getUser() {
        if (user == null) {
            user = usersFacade.find(11l);
        }
        return user;
    }
    
    public boolean doLogin(String username, String password) {
        Users dbUser = usersFacade.findByUsername(username);
        if (dbUser != null) {
            if (dbUser.getPassword().equals(password)) {
                user = dbUser;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public void doLogout() {
        user = usersFacade.find(11l);
    }
}
