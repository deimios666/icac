package ro.deimios.icac.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;
import ro.deimios.icac.helpers.dateHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@RequestScoped
public class UtilityBean {

    String luna;
    String anul;
    static SelectItem[] statusList;

    static {
        statusList = new SelectItem[4];
        statusList[0] = new SelectItem("", "Toate");
        statusList[1] = new SelectItem(0, "Necompletat");
        statusList[2] = new SelectItem(1, "Nevalidat");
        statusList[3] = new SelectItem(2, "Validat");
    }

    /**
     * Creates a new instance of UtilityBean
     */
    public UtilityBean() {
    }

    public String getAnul() {
        if (anul == null) {
            anul = String.valueOf(dateHelper.getReportYear());
        }
        return anul;
    }

    public String getLuna() {
        if (luna == null) {
            luna = dateHelper.getReportMonthString();
        }
        return luna;
    }

    public SelectItem[] getStatusList() {
        return statusList;
    }
}
