package ro.deimios.icac.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import ro.deimios.icac.datatypes.PJData;
import ro.deimios.icac.entity.Completat;
import ro.deimios.icac.entity.Retea;
import ro.deimios.icac.facade.CompletatFacadeLocal;
import ro.deimios.icac.facade.ReteaFacadeLocal;
import ro.deimios.icac.helpers.dateHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@ViewScoped
public class UnitateLister implements Serializable {

    @EJB
    private CompletatFacadeLocal completatFacade;
    @EJB
    private ReteaFacadeLocal reteaFacade;
    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    private List<PJData> unitateList;
    private List<PJData> filteredUnitateList;
    private int lastAdminLevel = -1;
    private PJData selectedPJ;
    private String selectedName;
    private long selectedSirues;

    private int selectedId = -1;

    /**
     * Creates a new instance of UnitateLister
     */
    public UnitateLister() {
    }

    @PostConstruct
    public void loadData() {
        //UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        List<Retea> unitati;
        //if user level change we change the list
        if (userBean.getUser().getAdminlevel() != lastAdminLevel) {
            lastAdminLevel = userBean.getUser().getAdminlevel();
            if (unitateList == null) {
                unitateList = new ArrayList();
            } else {
                unitateList.clear();
            }
        }
        if (userBean.user.getAdminlevel() == 1) {
            unitati = reteaFacade.findAllPJForOperator(userBean.user.getOperatorId().getId());
        } else {
            unitati = reteaFacade.findAllPJ();
        }

        Iterator<Retea> reteaWalker = unitati.iterator();
        int i = 0;
        while (reteaWalker.hasNext()) {
            Retea unitate = reteaWalker.next();
            int stareU = 0;
            Completat stareUnitate = completatFacade.findByLunaAnSirues(unitate.getSirues(), dateHelper.getReportMonthInt(), dateHelper.getReportYear());
            if (stareUnitate != null) {
                stareU = stareUnitate.getBlocklevel();
            }
            unitateList.add(new PJData(i, unitate.getSirues(), unitate.getNume(), stareU));
            i++;
        }
    }

    public void reloadData() {
        unitateList.set(selectedPJ.getId(), new PJData(selectedPJ.getId(), selectedPJ.getSirues(), selectedPJ.getNume(), selectedPJ.getStare()));
        selectedPJ = unitateList.get(selectedPJ.getId());
    }

    public void deblockData() {
        completatFacade.deleteByLunaAnSiruesPJ(selectedPJ.getSirues(), dateHelper.getReportMonthInt(), dateHelper.getReportYear());
    }

    public boolean canEdit() {
        return userBean.getUser().getAdminlevel() >= selectedPJ.getStare();
    }

    public boolean canDeblock() {
        return (userBean.getUser().getAdminlevel() >= selectedPJ.getStare() && userBean.getUser().getAdminlevel() > 0);
    }

    public void showDetails(ActionEvent actionEvent) {
        selectedName = actionEvent.getComponent().getId();
    }

    public List<PJData> getUnitateList() {
        if (userBean.getUser().getAdminlevel() != lastAdminLevel) {
            loadData();
        }
        return unitateList;
    }

    public void saveSelectedPJ() {
        //UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        selectedPJ.saveData();
        selectedPJ.setStare(userBean.getUser().getAdminlevel() + 1);
    }

    public String outcome() {
        return "result";
    }

    public void selectPJ() {

        Iterator<PJData> iterator = unitateList.iterator();

        while (iterator.hasNext()) {
            PJData pjData = iterator.next();
            if (pjData.getSirues() == selectedSirues) {
                selectedPJ = pjData;
                return;
            }
        }
        selectedPJ = null;
        //selectedPJ = unitateList.get(selectedSirues);
    }

    public long getSelectedSirues() {
        return selectedSirues;
    }

    public void setSelectedSirues(int selectedSirues) {
        this.selectedSirues = selectedSirues;
    }

    public String getSelectedName() {
        return selectedName;
    }

    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }

    public PJData getSelectedPJ() {
        if (selectedPJ == null) {
            selectedPJ = unitateList.get(0);
        } else {
        }
        return selectedPJ;
    }

    public void setSelectedPJ(PJData selectedPJ) {
        this.selectedPJ = selectedPJ;
    }

    public void setUnitateList(List<PJData> unitateList) {
        this.unitateList = unitateList;
    }

    public List<PJData> getFilteredUnitateList() {
        return filteredUnitateList;
    }

    public void setFilteredUnitateList(List<PJData> filteredUnitateList) {
        this.filteredUnitateList = filteredUnitateList;
    }

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        if (this.selectedId != selectedId) {
            this.selectedId = selectedId;
            selectedPJ = unitateList.get(selectedId);
        }
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
