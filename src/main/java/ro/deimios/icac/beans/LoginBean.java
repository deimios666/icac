package ro.deimios.icac.beans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import ro.deimios.icac.helpers.BeanHelper;
import ro.deimios.icac.helpers.cryptHelper;

/**
 *
 * @author Deimios
 */
@ManagedBean
@RequestScoped
public class LoginBean implements Serializable {

    private String username;
    private String password;

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public boolean doLogin(ActionEvent actionEvent) {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (!userBean.doLogin(username, password)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Autentificare Eșuată", "Nume utilizator sau parolă greșită"));
            return false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Autentificare Reușită", "Operatorul " + userBean.getUser().getOperatorId().getNume() + " s-a autentificat"));
            return true;
        }
    }

    public void logout(ActionEvent actionEvent) {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        String operator = userBean.user.getOperatorId().getNume();
        userBean.doLogout();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Deautentificare Reușită", "Operatorul " + operator + " s-a deautentificat"));
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = cryptHelper.sha512(password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
