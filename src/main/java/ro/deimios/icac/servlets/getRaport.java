package ro.deimios.icac.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ro.deimios.icac.helpers.CellHelper;
import ro.deimios.icac.helpers.dateHelper;

/**
 *
 * @author Deimios
 */
@WebServlet(name = "getRaport", urlPatterns = {"/getRaport"})
public class getRaport extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(getRaport.class.getName());
    @PersistenceContext(unitName = "icacPU")
    private EntityManager em;
    @Resource
    private javax.transaction.UserTransaction utx;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //get current luna/an
            int luna = dateHelper.getReportMonthInt();
            int anul = dateHelper.getReportYear();
            int raportId = 0;
            try {
                raportId = Integer.valueOf(request.getParameter("r"));
            } catch (NumberFormatException ignored) {
            }

            try {
                luna = Integer.valueOf(request.getParameter("luna"));
            } catch (NumberFormatException ignored) {
            }

            try {
                anul = Integer.valueOf(request.getParameter("anul"));
            } catch (NumberFormatException ignored) {
            }

            String raportName = "";
            switch (raportId) {
                case 1:
                    raportName = "completare";
                    break;
                case 2:
                    raportName = "judetean";
                    break;
                case 3:
                    raportName = "pj";
                    break;
                case 4:
                    raportName = "ar";
                    break;
            }

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=raport_" + raportName + "_" + anul + "_" + luna + ".xlsx");
            ServletContext context = getServletContext();

            InputStream templateInput = null;
            XSSFWorkbook template = null;
            XSSFWorkbook w = null;
            Sheet s;
            String queryString;
            Query query;
            List<Object[]> results;
            int rowOffset;
            int colOffset;
            int rowWalker;
            int colWalker;
            int colStart;

            /* 
             * Solves a weird bug on linux servers. Details: http://hal-log.blogspot.com/2011_02_01_archive.html (JPN)
             */
            //WorkbookSettings settings = new WorkbookSettings();
            //settings.setWriteAccess(null);
            switch (raportId) {
                case 1:
                    //RAPORT completare pe judet
                    //read template
                    templateInput = context.getResourceAsStream("/report_templates/raport_completare_template.xlsx");
                    w = (XSSFWorkbook) WorkbookFactory.create(templateInput);
                    s = w.getSheetAt(0);
                    //write data
                    queryString = "select o.nume, pj.nume, ar.nume,ifnull(c.blocklevel,0) from retea ar left join (select retea_sirues, blocklevel from completat where anul=" + anul + " and luna=" + luna + ") c on ar.sirues=c.retea_sirues left join retea pj on ar.sirues_sup=pj.sirues left join operator o on o.id=pj.operator_id where ar.raport order by o.nume, pj.nume, ar.nume;";
                    query = em.createNativeQuery(queryString);

                    results = query.getResultList();
                    rowWalker = 1;
                    for (Object[] o : results) {
                        colWalker = 0;
                        for (Object c : o) {
                            if (colWalker < o.length - 1) {
                                CellHelper.setLabel(s, colWalker, rowWalker, (String) c);
                            } else {
                                CellHelper.setNumber(s, colWalker, rowWalker, Double.valueOf((Long) c));
                            }
                            colWalker++;
                        }
                        rowWalker++;
                    }
                    break;

                case 2:
                    //RAPORT pe judet
                    //write headers
                    //set offset to header size

                    templateInput = context.getResourceAsStream("/report_templates/raport_judetean_template.xlsx");
                    w = (XSSFWorkbook) WorkbookFactory.create(templateInput);
                    s = w.getSheetAt(0);

                    int headerRowOffset = 1;
                    int headerColOffset = 1;

                    rowOffset = headerRowOffset;
                    colOffset = headerColOffset;
                    //skip headers as we have that in the template

                    //write current date
                    CellHelper.setLabel(s, 0, 0, "" + anul + " " + dateHelper.getReportMonthString(luna));
                    //write data
                    queryString = "SELECT a.dataTypeId.id, a.dataTypeId.description, sum(a.value) FROM Absente a WHERE a.luna=:luna AND a.anul=:anul GROUP BY a.dataTypeId.id, a.dataTypeId.description ORDER BY a.dataTypeId.id";
                    query = em.createQuery(queryString);
                    query.setParameter("luna", luna);
                    query.setParameter("anul", anul);
                    results = query.getResultList();
                    int currentRow = 0;
                    int currentCol = 0;

                    for (Object[] o : results) {

                        Integer dtId = (Integer) o[0];
                        Long absSum = (Long) o[2];
                        if (dtId < 58) {
                            //loop through normal (1-57)
                            currentCol = (int) Math.floor((dtId - 1) / 19);
                            currentRow = (dtId - 1) % 19;
                            if (currentRow > 12) {
                                rowOffset = 5;
                            } else if (currentRow > 7) {
                                rowOffset = 4;
                            } else if (currentRow > 3) {
                                rowOffset = 3;
                            } else {
                                rowOffset = 2;
                            }
                        } else if (dtId >= 58 && dtId <= 60) {
                            //loop through clasa pregătitoare (58-60)
                            rowOffset = headerRowOffset;
                            colOffset = headerColOffset;
                            currentCol = dtId - 58; //(0-2)
                            currentRow = 0;
                        } else if (dtId > 60 && dtId < 70) {
                            //loop profesional
                            rowOffset = headerRowOffset + 24;
                            colOffset = headerColOffset;
                            currentRow = (dtId - 61) % 3;
                            currentCol = (dtId - 61) / 3;
                        }

                        CellHelper.setNumber(s, colOffset + currentCol, rowOffset + currentRow, Double.valueOf(absSum));
                    }
                    break;

                case 3:
                    //RAPORT pe PJ
                    templateInput = context.getResourceAsStream("/report_templates/raport_pj_template.xlsx");
                    w = (XSSFWorkbook) WorkbookFactory.create(templateInput);
                    s = w.getSheetAt(0);

                    rowOffset = 2;
                    colOffset = 1;

                    queryString = "select pj.nume, SUM(IF(a.data_type_id=58,a.value,0)) AS av58, SUM(IF(a.data_type_id=59,a.value,0)) AS av59, SUM(IF(a.data_type_id=60,a.value,0)) AS av60, SUM(IF(a.data_type_id=1,a.value,0)) AS av1,SUM(IF(a.data_type_id=20,a.value,0)) AS av20,SUM(IF(a.data_type_id=39,a.value,0)) AS av39, SUM(IF(a.data_type_id=2,a.value,0)) AS av2,SUM(IF(a.data_type_id=21,a.value,0)) AS av21,SUM(IF(a.data_type_id=40,a.value,0)) AS av40, SUM(IF(a.data_type_id=3,a.value,0)) AS av3,SUM(IF(a.data_type_id=22,a.value,0)) AS av22,SUM(IF(a.data_type_id=41,a.value,0)) AS av41, SUM(IF(a.data_type_id=4,a.value,0)) AS av4,SUM(IF(a.data_type_id=23,a.value,0)) AS av23,SUM(IF(a.data_type_id=42,a.value,0)) AS av42, SUM(IF(a.data_type_id=5,a.value,0)) AS av5,SUM(IF(a.data_type_id=24,a.value,0)) AS av24,SUM(IF(a.data_type_id=43,a.value,0)) AS av43, SUM(IF(a.data_type_id=6,a.value,0)) AS av6,SUM(IF(a.data_type_id=25,a.value,0)) AS av25,SUM(IF(a.data_type_id=44,a.value,0)) AS av44, SUM(IF(a.data_type_id=7,a.value,0)) AS av7,SUM(IF(a.data_type_id=26,a.value,0)) AS av26,SUM(IF(a.data_type_id=45,a.value,0)) AS av45, SUM(IF(a.data_type_id=8,a.value,0)) AS av8,SUM(IF(a.data_type_id=27,a.value,0)) AS av27,SUM(IF(a.data_type_id=46,a.value,0)) AS av46, SUM(IF(a.data_type_id=9,a.value,0)) AS av9,SUM(IF(a.data_type_id=28,a.value,0)) AS av28,SUM(IF(a.data_type_id=47,a.value,0)) AS av47, SUM(IF(a.data_type_id=10,a.value,0)) AS av10,SUM(IF(a.data_type_id=29,a.value,0)) AS av29,SUM(IF(a.data_type_id=48,a.value,0)) AS av48, SUM(IF(a.data_type_id=11,a.value,0)) AS av11,SUM(IF(a.data_type_id=30,a.value,0)) AS av30,SUM(IF(a.data_type_id=49,a.value,0)) AS av49, SUM(IF(a.data_type_id=12,a.value,0)) AS av12,SUM(IF(a.data_type_id=31,a.value,0)) AS av31,SUM(IF(a.data_type_id=50,a.value,0)) AS av50, SUM(IF(a.data_type_id=13,a.value,0)) AS av13,SUM(IF(a.data_type_id=32,a.value,0)) AS av32,SUM(IF(a.data_type_id=51,a.value,0)) AS av51, SUM(IF(a.data_type_id=14,a.value,0)) AS av14,SUM(IF(a.data_type_id=33,a.value,0)) AS av33,SUM(IF(a.data_type_id=52,a.value,0)) AS av52, SUM(IF(a.data_type_id=15,a.value,0)) AS av15,SUM(IF(a.data_type_id=34,a.value,0)) AS av34,SUM(IF(a.data_type_id=53,a.value,0)) AS av53, SUM(IF(a.data_type_id=16,a.value,0)) AS av16,SUM(IF(a.data_type_id=35,a.value,0)) AS av35,SUM(IF(a.data_type_id=54,a.value,0)) AS av54, SUM(IF(a.data_type_id=17,a.value,0)) AS av17,SUM(IF(a.data_type_id=36,a.value,0)) AS av36,SUM(IF(a.data_type_id=55,a.value,0)) AS av55, SUM(IF(a.data_type_id=18,a.value,0)) AS av18,SUM(IF(a.data_type_id=37,a.value,0)) AS av37,SUM(IF(a.data_type_id=56,a.value,0)) AS av56, SUM(IF(a.data_type_id=19,a.value,0)) AS av19,SUM(IF(a.data_type_id=38,a.value,0)) AS av38,SUM(IF(a.data_type_id=57,a.value,0)) AS av57,SUM(IF(a.data_type_id=61,a.value,0)) AS av61,SUM(IF(a.data_type_id=64,a.value,0)) AS av64,SUM(IF(a.data_type_id=67,a.value,0)) AS av67,SUM(IF(a.data_type_id=62,a.value,0)) AS av62,SUM(IF(a.data_type_id=65,a.value,0)) AS av65,SUM(IF(a.data_type_id=68,a.value,0)) AS av68,SUM(IF(a.data_type_id=63,a.value,0)) AS av63,SUM(IF(a.data_type_id=66,a.value,0)) AS av66,SUM(IF(a.data_type_id=69,a.value,0)) AS av69 FROM retea ar  LEFT JOIN retea pj ON ar.sirues_sup=pj.sirues  LEFT JOIN absente a ON a.retea_sirues=ar.sirues WHERE a.luna=" + luna + " AND a.anul=" + anul + " AND ar.raport=true GROUP BY pj.nume ORDER BY pj.nume";
                    query = em.createNativeQuery(queryString);
                    results = query.getResultList();

                    //write data
                    rowWalker = 0;
                    for (Object[] o : results) {
                        CellHelper.setNumber(s, 0, rowWalker + rowOffset, Double.valueOf(rowWalker + 1));
                        colWalker = 0;
                        for (Object c : o) {

                            if (colWalker < 1) {
                                CellHelper.setLabel(s, colWalker + colOffset, rowWalker + rowOffset, (String) c);
                            } else {
                                CellHelper.setNumber(s, colWalker + colOffset, rowWalker + rowOffset, Double.valueOf(((BigDecimal) c).toPlainString()));
                            }
                            colWalker++;
                        }
                        rowWalker++;
                    }

                    break;

                case 4:
                    //RAPORT pe Structuri

                    templateInput = context.getResourceAsStream("/report_templates/raport_ar_template_v2.xlsx");
                    w = (XSSFWorkbook) WorkbookFactory.create(templateInput);
                    s = w.getSheetAt(0);

                    rowOffset = 2;
                    colOffset = 1;
                    colStart = 4;

                    queryString = "select pj.nume, ar.nume, ar.tip_idiot, ar.mediu ,SUM(IF(a.data_type_id=58,a.value,0)) AS av58, SUM(IF(a.data_type_id=59,a.value,0)) AS av59, SUM(IF(a.data_type_id=60,a.value,0)) AS av60, SUM(IF(a.data_type_id=1,a.value,0)) AS av1, SUM(IF(a.data_type_id=20,a.value,0)) AS av20,SUM(IF(a.data_type_id=39,a.value,0)) AS av39, SUM(IF(a.data_type_id=2,a.value,0)) AS av2,SUM(IF(a.data_type_id=21,a.value,0)) AS av21,SUM(IF(a.data_type_id=40,a.value,0)) AS av40, SUM(IF(a.data_type_id=3,a.value,0)) AS av3,SUM(IF(a.data_type_id=22,a.value,0)) AS av22,SUM(IF(a.data_type_id=41,a.value,0)) AS av41, SUM(IF(a.data_type_id=4,a.value,0)) AS av4,SUM(IF(a.data_type_id=23,a.value,0)) AS av23,SUM(IF(a.data_type_id=42,a.value,0)) AS av42, SUM(IF(a.data_type_id=5,a.value,0)) AS av5,SUM(IF(a.data_type_id=24,a.value,0)) AS av24,SUM(IF(a.data_type_id=43,a.value,0)) AS av43, SUM(IF(a.data_type_id=6,a.value,0)) AS av6,SUM(IF(a.data_type_id=25,a.value,0)) AS av25,SUM(IF(a.data_type_id=44,a.value,0)) AS av44, SUM(IF(a.data_type_id=7,a.value,0)) AS av7,SUM(IF(a.data_type_id=26,a.value,0)) AS av26,SUM(IF(a.data_type_id=45,a.value,0)) AS av45, SUM(IF(a.data_type_id=8,a.value,0)) AS av8,SUM(IF(a.data_type_id=27,a.value,0)) AS av27,SUM(IF(a.data_type_id=46,a.value,0)) AS av46, SUM(IF(a.data_type_id=9,a.value,0)) AS av9,SUM(IF(a.data_type_id=28,a.value,0)) AS av28,SUM(IF(a.data_type_id=47,a.value,0)) AS av47, SUM(IF(a.data_type_id=10,a.value,0)) AS av10,SUM(IF(a.data_type_id=29,a.value,0)) AS av29,SUM(IF(a.data_type_id=48,a.value,0)) AS av48, SUM(IF(a.data_type_id=11,a.value,0)) AS av11,SUM(IF(a.data_type_id=30,a.value,0)) AS av30,SUM(IF(a.data_type_id=49,a.value,0)) AS av49, SUM(IF(a.data_type_id=12,a.value,0)) AS av12,SUM(IF(a.data_type_id=31,a.value,0)) AS av31,SUM(IF(a.data_type_id=50,a.value,0)) AS av50, SUM(IF(a.data_type_id=13,a.value,0)) AS av13,SUM(IF(a.data_type_id=32,a.value,0)) AS av32,SUM(IF(a.data_type_id=51,a.value,0)) AS av51, SUM(IF(a.data_type_id=14,a.value,0)) AS av14,SUM(IF(a.data_type_id=33,a.value,0)) AS av33,SUM(IF(a.data_type_id=52,a.value,0)) AS av52, SUM(IF(a.data_type_id=15,a.value,0)) AS av15,SUM(IF(a.data_type_id=34,a.value,0)) AS av34,SUM(IF(a.data_type_id=53,a.value,0)) AS av53, SUM(IF(a.data_type_id=16,a.value,0)) AS av16,SUM(IF(a.data_type_id=35,a.value,0)) AS av35,SUM(IF(a.data_type_id=54,a.value,0)) AS av54, SUM(IF(a.data_type_id=17,a.value,0)) AS av17,SUM(IF(a.data_type_id=36,a.value,0)) AS av36,SUM(IF(a.data_type_id=55,a.value,0)) AS av55, SUM(IF(a.data_type_id=18,a.value,0)) AS av18,SUM(IF(a.data_type_id=37,a.value,0)) AS av37,SUM(IF(a.data_type_id=56,a.value,0)) AS av56, SUM(IF(a.data_type_id=19,a.value,0)) AS av19,SUM(IF(a.data_type_id=38,a.value,0)) AS av38,SUM(IF(a.data_type_id=57,a.value,0)) AS av57,SUM(IF(a.data_type_id=61,a.value,0)) AS av61,SUM(IF(a.data_type_id=64,a.value,0)) AS av64,SUM(IF(a.data_type_id=67,a.value,0)) AS av67,SUM(IF(a.data_type_id=62,a.value,0)) AS av62,SUM(IF(a.data_type_id=65,a.value,0)) AS av65,SUM(IF(a.data_type_id=68,a.value,0)) AS av68,SUM(IF(a.data_type_id=63,a.value,0)) AS av63,SUM(IF(a.data_type_id=66,a.value,0)) AS av66,SUM(IF(a.data_type_id=69,a.value,0)) AS av69 FROM retea ar  LEFT JOIN retea pj ON ar.sirues_sup=pj.sirues  LEFT JOIN absente a ON a.retea_sirues=ar.sirues WHERE a.luna=" + luna + " AND a.anul=" + anul + " AND ar.raport=true GROUP BY pj.nume, ar.nume, ar.tip_idiot, ar.mediu ORDER BY pj.nume, ar.nume";
                    query = em.createNativeQuery(queryString);
                    results = query.getResultList();

                    //write data
                    rowWalker = 0;
                    for (Object[] o : results) {
                        CellHelper.setNumber(s, 0, rowWalker + rowOffset, Double.valueOf(rowWalker + 1));
                        colWalker = 0;
                        for (Object c : o) {

                            if (colWalker < colStart) {
                                CellHelper.setLabel(s, colWalker + colOffset, rowWalker + rowOffset, (String) c);
                            } else {
                                CellHelper.setNumber(s, colWalker + colOffset, rowWalker + rowOffset, Double.valueOf(((BigDecimal) c).toPlainString()));
                            }
                            colWalker++;
                        }
                        rowWalker++;
                    }
                    break;
            }

            if (template != null) {
                template.close();
            }
            if (templateInput != null) {
                templateInput.close();
            }
            if (w != null) {
                XSSFFormulaEvaluator.evaluateAllFormulaCells(w);
                try (OutputStream out = response.getOutputStream()) {
                    w.write(out);
                    out.flush();
                }
            }

        } catch (InvalidFormatException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } finally {
            //out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void persist(Object object) {
        try {
            utx.begin();
            em.persist(object);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
            LOG.log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }
}
