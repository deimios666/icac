package ro.deimios.icac.servlets;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.extensions.ExtensionsRegistryFactory;
import ro.deimios.icac.helpers.dateHelper;

/**
 *
 * @author Deimios
 */
@WebServlet(name = "getPDF", urlPatterns = {"/getPDF"})
public class getPDF extends HttpServlet {

    @Resource(mappedName = "jdbc/icac")
    DataSource dataSource;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"Adresa_" + Integer.valueOf(request.getParameter("s")) + ".pdf\"");
        ServletContext context = getServletContext();
        try {
            try {
                InputStream inputStream = context.getResourceAsStream("/report_templates/adresa_absente.jasper");
                JasperReport jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
                Map<String, Object> params = new HashMap<>();
                params.put("anul", String.valueOf(dateHelper.getReportYear()));
                params.put("luna", dateHelper.getReportMonthString());
                params.put("sirues_unitate", new Integer(Integer.valueOf(request.getParameter("s"))));
                Connection conn = dataSource.getConnection();
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
                conn.close();
                ServletOutputStream servletOutPutStream = response.getOutputStream();
                byte[] buffer = JasperExportManager.exportReportToPdf(jasperPrint);
                servletOutPutStream.write(buffer, 0, buffer.length);
            } catch (SQLException | JRException ex) {
                Logger.getLogger(getPDF.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
