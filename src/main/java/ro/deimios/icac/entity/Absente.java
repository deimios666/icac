/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "absente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Absente.findAll", query = "SELECT a FROM Absente a"),
    @NamedQuery(name = "Absente.findByAnLunaSirues", query = "SELECT a FROM Absente a WHERE a.luna=:luna AND a.anul=:anul AND a.reteaSirues.sirues=:sirues"),
    @NamedQuery(name = "Absente.deleteByAnLunaSirues", query = "DELETE FROM Absente a WHERE a.luna=:luna AND a.anul=:anul AND a.reteaSirues.sirues=:sirues")
})
public class Absente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anul")
    private int anul;
    @Basic(optional = false)
    @NotNull
    @Column(name = "luna")
    private int luna;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value")
    private int value;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date stamp;
    @JoinColumn(name = "retea_sirues", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Retea reteaSirues;
    @JoinColumn(name = "data_type_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DataType dataTypeId;

    public Absente() {
    }

    public Absente(Long id) {
        this.id = id;
    }

    public Absente(Long id, int anul, int luna, int value, Date stamp) {
        this.id = id;
        this.anul = anul;
        this.luna = luna;
        this.value = value;
        this.stamp = stamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAnul() {
        return anul;
    }

    public void setAnul(int anul) {
        this.anul = anul;
    }

    public int getLuna() {
        return luna;
    }

    public void setLuna(int luna) {
        this.luna = luna;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getStamp() {
        return stamp;
    }

    public void setStamp(Date stamp) {
        this.stamp = stamp;
    }

    public Retea getReteaSirues() {
        return reteaSirues;
    }

    public void setReteaSirues(Retea reteaSirues) {
        this.reteaSirues = reteaSirues;
    }

    public DataType getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(DataType dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Absente)) {
            return false;
        }
        Absente other = (Absente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.icac.entity.Absente[ id=" + id + " ]";
    }
}
