/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "retea")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retea.findAll", query = "SELECT r FROM Retea r"),
    @NamedQuery(name = "Retea.findAllPJ", query = "SELECT r FROM Retea r WHERE r.pj=true AND r.raport=true ORDER BY r.nume"),
    @NamedQuery(name = "Retea.findAllPJForOperator", query = "SELECT r FROM Retea r WHERE r.pj=true AND r.raport=true AND r.operatorId.id=:operatorId ORDER BY r.nume"),
    @NamedQuery(name = "Retea.findAllARForPJ", query = "SELECT r FROM Retea r WHERE r.raport=true AND r.siruesSup.sirues=:sirues ORDER BY r.nume")
})
public class Retea implements Serializable {

    @Column(name = "old_sirues")
    private Long oldSirues;
    @Size(max = 40)
    @Column(name = "mediu")
    private String mediu;
    @Size(max = 45)
    @Column(name = "tip_idiot")
    private String tipIdiot;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "sirues")
    private Long sirues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "nume")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Column(name = "raport")
    private boolean raport;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pj")
    private boolean pj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "category")
    private int category;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reteaSirues")
    private Collection<Completat> completatCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siruesSup")
    private Collection<Retea> reteaCollection;
    @JoinColumn(name = "sirues_sup", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Retea siruesSup;
    @JoinColumn(name = "operator_id", referencedColumnName = "id")
    @ManyToOne
    private Operator operatorId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reteaSirues")
    private Collection<Absente> absenteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "centruSirues")
    private Collection<Operator> operatorCollection;

    public Retea() {
    }

    public Retea(Long sirues) {
        this.sirues = sirues;
    }

    public Retea(Long sirues, String nume, boolean raport, boolean pj, int category) {
        this.sirues = sirues;
        this.nume = nume;
        this.raport = raport;
        this.pj = pj;
        this.category = category;
    }

    public Long getSirues() {
        return sirues;
    }

    public void setSirues(Long sirues) {
        this.sirues = sirues;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public boolean getRaport() {
        return raport;
    }

    public void setRaport(boolean raport) {
        this.raport = raport;
    }

    public boolean getPj() {
        return pj;
    }

    public void setPj(boolean pj) {
        this.pj = pj;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @XmlTransient
    public Collection<Completat> getCompletatCollection() {
        return completatCollection;
    }

    public void setCompletatCollection(Collection<Completat> completatCollection) {
        this.completatCollection = completatCollection;
    }

    @XmlTransient
    public Collection<Retea> getReteaCollection() {
        return reteaCollection;
    }

    public void setReteaCollection(Collection<Retea> reteaCollection) {
        this.reteaCollection = reteaCollection;
    }

    public Retea getSiruesSup() {
        return siruesSup;
    }

    public void setSiruesSup(Retea siruesSup) {
        this.siruesSup = siruesSup;
    }

    public Operator getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Operator operatorId) {
        this.operatorId = operatorId;
    }

    @XmlTransient
    public Collection<Absente> getAbsenteCollection() {
        return absenteCollection;
    }

    public void setAbsenteCollection(Collection<Absente> absenteCollection) {
        this.absenteCollection = absenteCollection;
    }

    @XmlTransient
    public Collection<Operator> getOperatorCollection() {
        return operatorCollection;
    }

    public void setOperatorCollection(Collection<Operator> operatorCollection) {
        this.operatorCollection = operatorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sirues != null ? sirues.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retea)) {
            return false;
        }
        Retea other = (Retea) object;
        if ((this.sirues == null && other.sirues != null) || (this.sirues != null && !this.sirues.equals(other.sirues))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.icac.entity.Retea[ sirues=" + sirues + " ]";
    }

    public Long getOldSirues() {
        return oldSirues;
    }

    public void setOldSirues(Long oldSirues) {
        this.oldSirues = oldSirues;
    }

    public String getMediu() {
        return mediu;
    }

    public void setMediu(String mediu) {
        this.mediu = mediu;
    }

    public String getTipIdiot() {
        return tipIdiot;
    }

    public void setTipIdiot(String tipIdiot) {
        this.tipIdiot = tipIdiot;
    }
}
