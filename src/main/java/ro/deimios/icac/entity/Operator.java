/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "operator")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operator.findAll", query = "SELECT o FROM Operator o")})
public class Operator implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Size(max = 250)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "operatorId")
    private Collection<Users> usersCollection;
    @OneToMany(mappedBy = "operatorId")
    private Collection<Retea> reteaCollection;
    @JoinColumn(name = "centru_sirues", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Retea centruSirues;

    public Operator() {
    }

    public Operator(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @XmlTransient
    public Collection<Retea> getReteaCollection() {
        return reteaCollection;
    }

    public void setReteaCollection(Collection<Retea> reteaCollection) {
        this.reteaCollection = reteaCollection;
    }

    public Retea getCentruSirues() {
        return centruSirues;
    }

    public void setCentruSirues(Retea centruSirues) {
        this.centruSirues = centruSirues;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operator)) {
            return false;
        }
        Operator other = (Operator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.icac.entity.Operator[ id=" + id + " ]";
    }
    
}
