/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.icac.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Deimios
 */
@Entity
@Table(name = "completat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Completat.findAll", query = "SELECT c FROM Completat c"),
    @NamedQuery(name = "Completat.findByLunaAnSirues", query = "SELECT c FROM Completat c WHERE c.luna=:luna AND c.anul=:anul AND c.reteaSirues.sirues=:sirues"),
    @NamedQuery(name = "Completat.deleteByLunaAnSirues", query = "DELETE FROM Completat c WHERE c.luna=:luna AND c.anul=:anul AND c.reteaSirues.sirues=:sirues"),
    @NamedQuery(name = "Completat.findByLunaAnSiruesPJ", query = "SELECT c FROM Completat c WHERE c.luna=:luna AND c.anul=:anul AND c.reteaSirues.siruesSup.sirues=:sirues"),
    @NamedQuery(name = "Completat.deleteByLunaAnSiruesPJ", query = "DELETE FROM Completat c WHERE c.luna=:luna AND c.anul=:anul AND c.reteaSirues.siruesSup.sirues=:sirues")
})
public class Completat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anul")
    private int anul;
    @Basic(optional = false)
    @NotNull
    @Column(name = "luna")
    private int luna;
    @Basic(optional = false)
    @NotNull
    @Column(name = "blocklevel")
    private int blocklevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "blocked_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date blockedTime;
    @JoinColumn(name = "retea_sirues", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Retea reteaSirues;

    public Completat() {
    }

    public Completat(Long id) {
        this.id = id;
    }

    public Completat(Long id, int anul, int luna, int blocklevel, Date blockedTime) {
        this.id = id;
        this.anul = anul;
        this.luna = luna;
        this.blocklevel = blocklevel;
        this.blockedTime = blockedTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAnul() {
        return anul;
    }

    public void setAnul(int anul) {
        this.anul = anul;
    }

    public int getLuna() {
        return luna;
    }

    public void setLuna(int luna) {
        this.luna = luna;
    }

    public int getBlocklevel() {
        return blocklevel;
    }

    public void setBlocklevel(int blocklevel) {
        this.blocklevel = blocklevel;
    }

    public Date getBlockedTime() {
        return blockedTime;
    }

    public void setBlockedTime(Date blockedTime) {
        this.blockedTime = blockedTime;
    }

    public Retea getReteaSirues() {
        return reteaSirues;
    }

    public void setReteaSirues(Retea reteaSirues) {
        this.reteaSirues = reteaSirues;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Completat)) {
            return false;
        }
        Completat other = (Completat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.icac.entity.Completat[ id=" + id + " ]";
    }
    
}
