package ro.deimios.icac.datatypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.icac.entity.Retea;
import ro.deimios.icac.facade.ReteaFacadeLocal;

/**
 *
 * @author Deimios
 */
public class PJData implements Serializable {

    ReteaFacadeLocal reteaFacade = lookupReteaFacadeLocal();
    private long sirues;
    private String nume;
    private int stare;
    private int id;
    private List<UnitateData> data;

    public PJData() {
    }

    public PJData(long sirues, String nume, int stare) {
        this.sirues = sirues;
        this.stare = stare;
        this.nume = nume;
    }

    public PJData(int id, long sirues, String nume, int stare) {
        this.id = id;
        this.sirues = sirues;
        this.stare = stare;
        this.nume = nume;
    }

    public List<UnitateData> getData() {
        if (data == null) {
            data = new ArrayList();
            Collection<Retea> unitati = reteaFacade.findAllARForPJ(sirues);
            Iterator<Retea> unitatiWalker = unitati.iterator();
            while (unitatiWalker.hasNext()) {
                Retea unitate = unitatiWalker.next();
                data.add(new UnitateData(unitate.getSirues(), unitate.getCategory()));
            }
        }
        return data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void saveData() {
        for (int i = 0; i < data.size(); i++) {
            data.get(i).saveData();
        }
    }

    public long getSirues() {
        return sirues;
    }

    public void setData(List<UnitateData> data) {
        this.data = data;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getStare() {
        return stare;
    }

    public void setStare(int stare) {
        this.stare = stare;
    }

    private ReteaFacadeLocal lookupReteaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ReteaFacadeLocal) c.lookup("java:global/icac/ReteaFacade!ro.deimios.icac.facade.ReteaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
