package ro.deimios.icac.datatypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.icac.beans.UserBean;
import ro.deimios.icac.entity.Absente;
import ro.deimios.icac.entity.Completat;
import ro.deimios.icac.facade.AbsenteFacadeLocal;
import ro.deimios.icac.facade.CompletatFacadeLocal;
import ro.deimios.icac.facade.DataTypeFacadeLocal;
import ro.deimios.icac.facade.ReteaFacadeLocal;
import ro.deimios.icac.helpers.BeanHelper;
import ro.deimios.icac.helpers.dateHelper;

/**
 *
 * @author Deimios
 */
public class UnitateData implements Serializable {

    CompletatFacadeLocal completatFacade = lookupCompletatFacadeLocal();
    ReteaFacadeLocal reteaFacade = lookupReteaFacadeLocal();
    DataTypeFacadeLocal dataTypeFacade = lookupDataTypeFacadeLocal();
    AbsenteFacadeLocal absenteFacade = lookupAbsenteFacadeLocal();
    private long sirues;
    private String nume;
    private int category;
    private List<String> data;

    public UnitateData(long sirues, int category) {
        this.category = category;
        this.sirues = sirues;
    }

    public List<String> getData() {
        if (data == null) {
            data = new ArrayList();

            int dataTypeSize=dataTypeFacade.count();
            
            for (int i = 0; i < dataTypeSize; i++) {
                data.add(i, "0");
            }

            List<Absente> absente = absenteFacade.findByAnLunaSirues(dateHelper.getReportYear(), dateHelper.getReportMonthInt(), sirues);
            Iterator<Absente> absenteWalker = absente.iterator();
            while (absenteWalker.hasNext()) {
                Absente absenta = absenteWalker.next();
                data.set(absenta.getDataTypeId().getId() - 1, String.valueOf(absenta.getValue()));
            }
        }
        return data;
    }

    public void setData(List<String> data) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isEmpty()) {
                this.data.set(i, "0");
            } else {
                this.data.set(i, data.get(i));
            }
        }
        this.data = data;
    }

    public void saveData() {
        //update completat, if adminlevel is high enough
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        int blockLevel = userBean.getUser().getAdminlevel() + 1;
        if (blockLevel == 9) {
            blockLevel = 2;
        }
        Completat completat = completatFacade.findByLunaAnSirues(sirues, dateHelper.getReportMonthInt(), dateHelper.getReportYear());
        if (completat == null) {
            completat = new Completat(null, dateHelper.getReportYear(), dateHelper.getReportMonthInt(), blockLevel, new Date());
            completat.setReteaSirues(reteaFacade.find(sirues));
            completatFacade.create(completat);
        } else {
            if (userBean.getUser().getAdminlevel() >= completat.getBlocklevel()) {
                completat.setBlocklevel(blockLevel);
                completatFacade.edit(completat);
            } else {
                return;
            }
        }
        //delete older data
        absenteFacade.deleteByAnLunaSirues(dateHelper.getReportYear(), dateHelper.getReportMonthInt(), sirues);
        //write new data
        for (int i = 0; i < data.size(); i++) {
            int currData = 0;
            try {
                if (!data.get(i).isEmpty()) {
                    currData = Integer.valueOf(data.get(i).trim());
                }
            } catch (NumberFormatException ex) {
            }
            if (currData > 0) {
                Absente absente = new Absente(null, dateHelper.getReportYear(), dateHelper.getReportMonthInt(), currData, new Date());
                absente.setDataTypeId(dataTypeFacade.find(i + 1));
                absente.setReteaSirues(reteaFacade.find(sirues));
                absenteFacade.create(absente);
            }
        }
    }

    public long getSirues() {
        return sirues;
    }

    public String getNume() {
        if (nume == null) {
            nume = reteaFacade.find(sirues).getNume();
        }
        return nume;
    }

    public int getCategory() {
        return category;
    }

    private AbsenteFacadeLocal lookupAbsenteFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (AbsenteFacadeLocal) c.lookup("java:global/icac/AbsenteFacade!ro.deimios.icac.facade.AbsenteFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private DataTypeFacadeLocal lookupDataTypeFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (DataTypeFacadeLocal) c.lookup("java:global/icac/DataTypeFacade!ro.deimios.icac.facade.DataTypeFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private ReteaFacadeLocal lookupReteaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ReteaFacadeLocal) c.lookup("java:global/icac/ReteaFacade!ro.deimios.icac.facade.ReteaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private CompletatFacadeLocal lookupCompletatFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (CompletatFacadeLocal) c.lookup("java:global/icac/CompletatFacade!ro.deimios.icac.facade.CompletatFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
