package ro.deimios.icac.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Deimios
 */
public class cryptHelper {

    public static String sha512(String value) {
        String pHash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] byteData = md.digest(value.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            pHash = hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(cryptHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pHash;
    }
}
