package ro.deimios.icac.helpers;

import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Deimios
 */
public class CellHelper {

    public static void setLabel(Sheet sheet, int column, int row, String value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }
        Cell cell = rowObj.getCell(column);

        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value);
    }

    public static void setNumber(Sheet sheet, int column, int row, Double value) {
        Row rowObj = sheet.getRow(row);
        if (rowObj == null) {
            rowObj = sheet.createRow(row);
        }
        Cell cell = rowObj.getCell(column);

        if (cell == null) {
            cell = rowObj.createCell(column);
        }
        cell.setCellValue(value);
    }
    private static final Logger LOG = Logger.getLogger(CellHelper.class.getName());
}
