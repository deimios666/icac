package ro.deimios.icac.helpers;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author Deimios
 */
public class dateHelper {

    public static int getReportYear() {
        int currYear = Calendar.getInstance().get(Calendar.YEAR);
        if (Calendar.getInstance().get(Calendar.MONTH) > 0) {
            return currYear;
        } else {
            return currYear - 1;
        }
    }

    public static String getReportMonthString() {
        DateFormatSymbols dfs = new DateFormatSymbols(new Locale("ro"));
        String[] months = dfs.getMonths();
        if (Calendar.getInstance().get(Calendar.MONTH) > 0) {
            return months[Calendar.getInstance().get(Calendar.MONTH) - 1];
        } else {
            return months[11];
        }
    }

    public static String getReportMonthString(int month) {
        DateFormatSymbols dfs = new DateFormatSymbols(new Locale("ro"));
        String[] months = dfs.getMonths();
        return months[(month - 1)];
    }

    public static int getReportMonthInt() {
        if (Calendar.getInstance().get(Calendar.MONTH) > 0) {
            return Calendar.getInstance().get(Calendar.MONTH);
        } else {
            return 12;
        }
    }
}
