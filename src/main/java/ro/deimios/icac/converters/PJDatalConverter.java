package ro.deimios.icac.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import ro.deimios.icac.datatypes.PJData;

/**
 *
 * @author Deimios
 */
@FacesConverter(value = "PJDataConverter")
public class PJDatalConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String[] parts = value.split("\\|");
        //return new Copil(Long.valueOf(parts[0]), parts[1], parts[2], true);
        return new PJData(Integer.parseInt(parts[0]), parts[1], Integer.parseInt(parts[2]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        PJData pjData = (PJData) value;
        return pjData.getSirues() + "|" + pjData.getNume() + "|" + pjData.getStare();
    }
}
